// make an mock data for an example 

const courseData = [
    {
        id:"wsc001",
        name: "PHP-laravel",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris dignissim sodales auctor. Nam vulputate ut elit faucibus rutrum. Donec non lacus quis est suscipit porta nec nec augue. Pellentesque accumsan erat lacus, ac pulvinar purus semper ac. Nullam laoreet imperdiet bibendum. Ut lacinia libero vel urna rutrum egestas. Nam ornare non metus eget volutpat. Fusce aliquam accumsan efficitur. Nulla vestibulum arcu eu leo placerat lacinia. Integer sapien enim, feugiat non sem ac, maximus iaculis sapien. Aenean pretium tempor tellus ut auctor. Aenean magna diam, aliquam sed laoreet quis, pretium in est. Donec eu placerat nisl. Cras lobortis diam tellus, vel euismod massa scelerisque et.",
        price:45000,
        onOffer:true
    },
    {
        id:"wsc002",
        name: "python-Django",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris dignissim sodales auctor. Nam vulputate ut elit faucibus rutrum. Donec non lacus quis est suscipit porta nec nec augue. Pellentesque accumsan erat lacus, ac pulvinar purus semper ac. Nullam laoreet imperdiet bibendum. Ut lacinia libero vel urna rutrum egestas. Nam ornare non metus eget volutpat. Fusce aliquam accumsan efficitur. Nulla vestibulum arcu eu leo placerat lacinia. Integer sapien enim, feugiat non sem ac, maximus iaculis sapien. Aenean pretium tempor tellus ut auctor. Aenean magna diam, aliquam sed laoreet quis, pretium in est. Donec eu placerat nisl. Cras lobortis diam tellus, vel euismod massa scelerisque et.",
        price:55000,
        onOffer:true
    },
    {
        id:"wsc003",
        name: "Java-Springboot",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris dignissim sodales auctor. Nam vulputate ut elit faucibus rutrum. Donec non lacus quis est suscipit porta nec nec augue. Pellentesque accumsan erat lacus, ac pulvinar purus semper ac. Nullam laoreet imperdiet bibendum. Ut lacinia libero vel urna rutrum egestas. Nam ornare non metus eget volutpat. Fusce aliquam accumsan efficitur. Nulla vestibulum arcu eu leo placerat lacinia. Integer sapien enim, feugiat non sem ac, maximus iaculis sapien. Aenean pretium tempor tellus ut auctor. Aenean magna diam, aliquam sed laoreet quis, pretium in est. Donec eu placerat nisl. Cras lobortis diam tellus, vel euismod massa scelerisque et.",
        price:45000,
        onOffer:true
    }
]
// export the data to Courses.js or other file 
export default courseData;