
import {Container} from 'react-bootstrap'
import AppNavbar from './components/AppNavbar'; // ->  -> import from components folder and determine the file specific
// import Banner from './components/Banner'; // ->
// import CourseCard  from "./components/CourseCard";
// import Highlights from './components/Highlights';
import Course from './pages/Courses';
import Home from './pages/Home';
import './App.css';

// /* <></> */ --> reat fragment
function App() {
  return (
    <> 

    <AppNavbar/>
      <Container>
            <Home/>
            {/* <CourseCard/> */}
            <Course/>
      </Container>
      
    </>
  )
}

export default App;
