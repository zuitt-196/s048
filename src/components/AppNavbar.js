import{Navbar, Container, Nav} from 'react-bootstrap' /// --> import object in comes to get some element from react-bootstrap
export default function  AppNavbar() {
        return (
            <Navbar bg="light" expand="lg">
            <Container>
              <Navbar.Brand href="#home">Batch-196 Booking App</Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                  <Nav.Link href="#home">Home</Nav.Link>
                  <Nav.Link href="#link">Coures</Nav.Link>
                </Nav>
              </Navbar.Collapse>
            </Container>
          </Navbar>
        )
};